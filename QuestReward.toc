## Interface: 70200
## Title: QuestReward
## Author: Kjasi
## Version: <%version%>
## Notes: Automatically Chooses the best Quest Reward, based on vendor value.
## URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Website: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Feedback: https://bitbucket.org/Kjasi/kjasis-wow-addons
## DefaultState: Enabled
## LoadOnDemand: 0
## SavedVariablesPerCharacter: QuestReward_Options
Libs\Libs.xml
main.lua
main.xml